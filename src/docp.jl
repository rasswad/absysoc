const Bound = Union{Nothing, Vector}
const TimeRange{T<:AbstractFloat} = Union{AbstractRange{T}, AbstractVector{T}}

struct DOCPSolution
    t::TimeRange
    x::Matrix{Real}
    λ::Matrix{Real}
    u::Matrix{Real}
    obj::Real
end

TimeRange(t0::T, tf::T, N::Ti) where {T<:AbstractFloat, Ti<:Integer} = t0:(tf-t0)/(N-1):tf
TimeRange(t0::Ti, tf::Ti, N::Ti) where {Ti<:Integer} = TimeRange(Float64(t0), Float64(tf), N)
time_bounds(t::TimeRange) = (t[1], t[end])
time_bounds(sol::DOCPSolution) = time_bounds(sol.t)
Base.length(sol::DOCPSolution) = length(sol.t)

function new_docp(state_dim::Integer, control_dim::Integer, dynamics_f::Function,
        time_steps::Integer, time_range, rk::rk_data=rk_data(:gauss2);
        objective_sense::Symbol=:max,
        state_lb::Bound=nothing, state_ub::Bound=nothing,
        control_lb::Bound=nothing, control_ub::Bound=nothing)
    docp = JuMP.Model(Ipopt.Optimizer)
    n = state_dim
    m = control_dim
    N = time_steps

    # define state
    if isnothing(state_lb)
        if isnothing(state_ub)
            @variable(docp, x[1:N, 1:n])
        else
            @variable(docp, x[1:N, i = 1:n] <= state_ub[i])
        end
    else
        if isnothing(state_ub)
            @variable(docp, x[1:N, i = 1:n] >= state_lb[i])
        else
            @variable(docp, state_lb[i] <= x[1:N, i = 1:n] <= state_ub[i])
        end
    end
    
    # define control
    if isnothing(control_lb)
        if isnothing(control_ub)
            @variable(docp, u[1:N, 1:m])
        else
            @variable(docp, u[1:N, i = 1:m] <= control_ub[i])
        end
    else
        if isnothing(control_ub)
            @variable(docp, u[1:N, i = 1:m] >= control_lb[i])
        else
            @variable(docp, control_lb[i] <= u[1:N, i = 1:m] <= control_ub[i])
        end
    end

    # define RK scheme
    autonomous_rk!(docp, rk, dynamics_f, state_dim, time_steps, time_range)

    # define objective
    if objective_sense == :max
        @objective(docp, Max, x[end, end])
    elseif objective_sense == :min
        @objective(docp, Min, x[end, end])
    else
        error("`objective_sense` must be :max or :min")
    end

    return docp
end

function set_initial_state!(docp::GenericModel, x0::Vector{<:Real})
    @constraint(docp, initial_state, docp.obj_dict[:x][1,:] == x0[:])
end

function solve!(docp::GenericModel, time_range, time_steps::Integer;
        tol=1e-12, max_iter=1000, print_level=4, mu_strategy="adaptive",
        log_path::Union{String,Nothing}=nothing)
    if log_path === nothing
        return _solve!(docp, time_range, time_steps; tol=tol,
            print_level=print_level, max_iter=max_iter, mu_strategy=mu_strategy)
    end
    local sol, msg
    time = @capture_out msg = @timev @capture_out begin
        sol = _solve!(docp, time_range, time_steps; tol=tol,
            print_level=print_level, max_iter=max_iter, mu_strategy=mu_strategy)
    end
    msg = msg * "\n" * time * "\n"
    open(log_path, "w") do f
        write(f, msg)
    end
    return sol
end

function _solve!(docp::GenericModel, time_range, time_steps::Integer;
        tol=1e-12, max_iter=1000, print_level=4, mu_strategy="adaptive")
    # set optimizer options
    set_optimizer_attribute(docp, "print_level", print_level)
    set_optimizer_attribute(docp, "tol", tol)
    set_optimizer_attribute(docp, "max_iter", max_iter)
    set_optimizer_attribute(docp, "mu_strategy", mu_strategy)
    
    # run solver
    optimize!(docp)

    # retrieve solution
    t0, tf = time_range
    t = TimeRange(t0, tf, time_steps)
    x = value.(docp.obj_dict[:x])
    u = value.(docp.obj_dict[:u])
    λ = dual.(docp.obj_dict[:rk_scheme])
    λ = -reduce(vcat, transpose.(λ))
    λ = vcat(λ, λ[end,:]')
    return DOCPSolution(t, x, λ, u, objective_value(docp))
end

# warm start

function set_initial_guess!(docp::GenericModel, u::AbstractMatrix, x::AbstractMatrix)
    new_N = size(docp.obj_dict[:u], 1)
    old_N = size(u, 1)
    x_docp = docp.obj_dict[:x]
    u_docp = docp.obj_dict[:u]
    @assert mod(new_N, old_N) == 0 || mod(old_N, new_N) == 0 "incompatible solution sizes"
    if new_N >= old_N
        skip = div(new_N, old_N)
        for i in 1:skip
            set_start_value.(x_docp[i:skip:end, :], x)
            set_start_value.(u_docp[i:skip:end, :], u)
        end
    else
        skip = div(old_N, new_N)
        for i in 1:skip
            set_start_value.(x_docp, x[i:skip:end, :])
            set_start_value.(u_docp, u[i:skip:end, :])
        end
    end
end

function set_initial_guess!(docp::GenericModel, sol::DOCPSolution)
    set_initial_guess!(docp, sol.u, sol.x)
end

function set_initial_guess!(docp::GenericModel, sol_path::String)
    x_name = sol_path * "/docp_state.array"
    u_name = sol_path * "/docp_control.array"
    set_initial_guess!(docp, readdlm(u_name), readdlm(x_name))
end

export Bound, TimeRange, DOCPSolution, time_bounds
export new_docp, set_initial_state!, solve!, set_initial_guess!