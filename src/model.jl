@kwdef struct ABmodel
    s_in::Real
    β::Real
    γ::Real
    ϕmax::Real
    ks::Real
    ρmax::Real
    kv::Real
    μmax::Real
    qmin::Real
end

# declare parameters as global variables
macro export_parameters(object)
    return quote
        for param in fieldnames(typeof($object))
            eval(:($param = $$object.$param))
            eval(:(export $param))
        end
    end
end

# Model parameters
ab_model = ABmodel(
    # System parameters
    s_in = 0.5,
    β = 23e-3,
    γ = 0.44,
    # Reaction rate parameters
    ϕmax = 6.48, ks = 0.09,
    ρmax = 27.3e-3, kv = 0.57e-3,
    μmax = 1.0211, qmin = 2.7628e-3
)

export ABmodel, ab_model, @export_parameters