function write_docp(sol::DOCPSolution;
        path::String="results", singular::Union{AbstractArray, Nothing},
        switch::Union{AbstractArray, Nothing})
    if path[end] != '/'
        path *= "/"
    end
    mkpath(path)
    fname(s) = path * "docp_" * string(length(sol.t)) * "_" * s * ".array"

    writedlm(fname("time"), sol.t)
    writedlm(fname("state"), sol.x)
    writedlm(fname("costate"), sol.λ)
    writedlm(fname("control"), sol.u)
    writedlm(fname("singular"), singular)
    writedlm(fname("switch"), switch)
end

function write_socp(sol::SOCPSolution; path::String="results",
        alpha::AbstractArray, d::AbstractArray,
        d_washout::AbstractArray, objective::AbstractArray)
    if path[end] != '/'
        path *= "/"
    end
    mkpath(path)
    fname(s) = path * s * ".array"
    
    writedlm(fname("socp_control"), sol.u)
    writedlm(fname("socp_objective"), sol.obj)
    writedlm(fname("alpha_mesh"), alpha)
    writedlm(fname("d_mesh"), d)
    writedlm(fname("d_washout"), d_washout)
    writedlm(fname("objective_mesh"), objective)
end

export write_docp, write_socp