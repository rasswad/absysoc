struct rk_data
    name::Symbol
    s::Integer
    a::Matrix{<:Real}
    b::Vector{<:Real}
    c::Vector{<:Real}
    properties::Dict{Symbol, Any}
end

rk_data(name, s, a, b, c) = rk_data(name, s, a, b, c, Dict{Symbol,Any}())

function rk_data(name::Symbol)
    if name == :midpoint || name == :gauss1
        s = 1
        a = reshape([0.5], s, s)
        b = [1]
        c = [0.5]
        prop = Dict(
            :description => "Implicit Midpoint Rule (Gauss-Legendre method: 1 stage, order 2)",
            :order => 2,
            :implicit => true
        )
    elseif name == :trapezoid
        s = 2
        a = [0 0; 0.5 0.5]
        b = [0.5, 0.5]
        c = [0, 1]
        prop = Dict(
            :description => "Implicit Trapezoidal Rule (Crank-Nicolson)",
            :order => 2,
            :implicit => true
        )
    elseif name == :gauss2
        s = 2
        a = [0.25 (0.25 - sqrt(3)/6); (0.25 + sqrt(3)/6) 0.25]
        b = [0.5, 0.5]
        c = [0.5 - sqrt(3)/6, 0.5 + sqrt(3)/6]
        prop = Dict(
            :description => "Gauss-Legendre method: 2 stages, order 4",
            :order => 4,
            :implicit => true
        )
    elseif name == :gauss3
        s = 3
        a = [
            5/36 2/9 - sqrt(15)/15 5/36 - sqrt(15)/30;
            5/36 + sqrt(15)/24 2/9 5/36 - sqrt(15)/24;
            5/36 + sqrt(15)/30 2/9 + sqrt(15)/15 5/36
        ]
        b = [5/18, 4/9, 5/18]
        c = [(0.5 - sqrt(15)/10), 0.5, (0.5 + sqrt(15)/10)]
        prop = Dict(
            :description => "Gauss-Legendre method: 3 stages, order 6",
            :order => 6,
            :implicit => true
        )
    else
        error("Unknown Runge-Kutta scheme " * string(name))
    end
    return rk_data(name, s, a, b, c)
end


# must be called after defining state and control
function autonomous_rk!(docp::GenericModel, rk::rk_data, dynamics_f::Function,
        state_dim::Integer, time_steps::Integer, time_range)
    f = dynamics_f
    n = state_dim
    N = time_steps

    x = docp.obj_dict[:x]
    u = docp.obj_dict[:u]
    
    t1, t2 = time_range
    Δt = (t2 - t1) / (time_steps - 1)

    # define k arrays as optimization variables
    @variable(docp, k[1:rk.s, 1:N, 1:n])

    # define RK scheme as nonlienar optimization constraint
    @constraints(docp, begin
        # k[j,i] = f( x[i] + Δt Σ_s A[j,s]k[s,i] )
        rk_inner[j=1:rk.s, i=1:N], k[j,i,:] == f(x[i,:] + Δt*sum(rk.a[j,s]*k[s,i,:] for s in 1:rk.s), u[i,:])
        # x[i+1] = x[i] + Δt Σ_j b[j]k[j,i]
        rk_scheme[i = 1:N-1], x[i+1,:] == x[i,:] + Δt*sum(rk.b[j] * k[j,i,:] for j in 1:rk.s)
    end)

    docp
end

# must be called after defining state and control
function timevarying_rk!(docp::GenericModel, rk::rk_data, dynamics_f::Function,
        state_dim::Integer, time_steps::Integer, time_range)
    f = dynamics_f
    n = state_dim
    N = time_steps

    x = docp.obj_dict[:x]
    u = docp.obj_dict[:u]
    
    t1, t2 = time_range
    Δt = (t2 - t1) / (time_steps - 1)

    # define k arrays as optimization variables
    @variable(docp, k[1:rk.s, 1:N, 1:n])

    # define RK scheme as nonlienar optimization constraint
    @constraints(docp, begin
        # k[j,i] = f( t0 + Δt c[j] , x[i] + Δt Σ_s A[j,s]k[s,i] )
        rk_inner[j=1:rk.s, i=1:N], k[j,i,:] == f(t1 + Δt*rk.c[j], x[i,:] + Δt*sum(rk.a[j,s]*k[s,i,:] for s in 1:rk.s), u[i,:])
        # x[i+1] = x[i] + Δt Σ_j b[j]k[j,i]
        rk_scheme[i = 1:N-1], x[i+1,:] == x[i,:] + Δt*sum(rk.b[j] * k[j,i,:] for j in 1:rk.s)
    end)

    docp
end

export rk_data, autonomous_rk!, timevarying_rk!