struct SOCPSolution
    u::Vector{Real}
    obj::Real
end

function new_socp(control_dim::Integer, lower::Vector{<:Real}, upper::Vector{<:Real})

    socp = JuMP.Model(Ipopt.Optimizer)
    @variable(socp, lower[i] <= u[i = 1:control_dim] <= upper[i])

    return socp
end

function solve!(socp::GenericModel; tol=1e-20, max_iter=1000, print_level=4,
        mu_strategy="adaptive", log_path::Union{String,Nothing}=nothing)
    if log_path === nothing
        return _solve!(socp; tol=tol, max_iter=max_iter,
            print_level=print_level, mu_strategy=mu_strategy)
    end
    local sol, msg
    time = @capture_out msg = @timev @capture_out begin
        sol = _solve!(socp; tol=tol, max_iter=max_iter,
            print_level=print_level, mu_strategy=mu_strategy)
    end
    msg = msg * "\n" * time * "\n"
    open(log_path, "w") do f
        write(f, msg)
    end
    return sol
end

function _solve!(socp::GenericModel;
        tol=1e-20, max_iter=1000, print_level=4, mu_strategy="adaptive")
    # set optimizer options
    set_optimizer_attribute(socp, "print_level", print_level)
    set_optimizer_attribute(socp, "tol", tol)
    set_optimizer_attribute(socp, "max_iter", max_iter)
    set_optimizer_attribute(socp, "mu_strategy", mu_strategy)
    
    # run solver
    optimize!(socp)

    return SOCPSolution(value.(socp.obj_dict[:u]), objective_value(socp))
end

export SOCPSolution, new_socp, solve!