# Plotting functions for Algal-Bacterial model

# Plots defaults
const PLOTS_DEFAULTS = Dict(
    :fontfamily => "Computer Modern",
    :label => nothing,
    :framestyle => :axes # in [:axes :box :semi :origin :zerolines :grid :none]
)
function plots_defaults(dict::Union{Nothing, Dict{Symbol, Any}}=PLOTS_DEFAULTS)
    Plots.default(; dict...)
end

# Shorthands
latexify(tab) = latexstring.(L"$" .* tab .* L"(t)$")
lab(tab) = latexify(reshape(tab[order], (1, length(tab))))
x_lab = ["s", "e", "v", "q", "c", ""]
u_lab = [raw"\alpha", "d"]
order = [1, 3, 4, 2, 5, 6]
c = [:blue, :magenta, :purple, :cyan, :lime, :orange]
l = (2, 3)
ft3(val) = string(val * 10^3)

# DOCP solution trajectories -----------------------------------------------

function plot_objective_trajectory(sol::DOCPSolution; kwargs...)
    p = plot(sol.t, sol.x[:,end]; fillrange=zeros(length(sol.t)), alpha=.5, kwargs...)
    mid = Integer(ceil(length(sol.t) * 0.75))
    xpos = sol.t[mid]
    ypos = sol.x[mid,end] / 2
    fnt = Plots.font(family="Computer Modern", pointsize=8)
    plot!(annotations=(xpos, ypos, Plots.text(round(sol.obj; digits=3), :hcenter, fnt)))
    xlims!(time_bounds(sol))
    ylims!(0, maximum(sol.x[:,end]))
    return p
end

function plot_state(sol::DOCPSolution)
    ps = Plots.Plot[]
    for i in 1:5
        push!(ps,
            plot(sol.t, sol.x[:,order[i]], label=latexify(x_lab[order[i]]),
            color=c[order[i]])
        )
        xlims!(time_bounds(sol))
        ylims!(0, 1.05 * maximum(sol.x[:,order[i]]))
        if i in [2,3] # v, q
            yaxis!(ft3)
            annotate!([(2.5, 1.1 * maximum(sol.x[:,order[i]]),
                Plots.text(L"\times10^{-3}", 6, :black, :center))])
        end
    end
    push!(ps, plot_objective_trajectory(sol; label=L"\int d(\tau)c(\tau)\mathrm{d}\tau", color=c[end]))
    plot(ps..., layout=(2, 3), size=(500, 300))
end

function plot_costate(sol::DOCPSolution)
    ps = Plots.Plot[]
    for i in 1:5
        push!(ps,
            plot(sol.t, sol.λ[:,order[i]], color=c[order[i]],
            label=latexify("\\lambda_" * x_lab[order[i]]))
        )
        xlims!(time_bounds(sol))
        ylims!(0, 1.05 * maximum(sol.λ[:,order[i]]))
        if i in []
            yaxis!(ft3)
            annotate!([(2.5, 1.1 * maximum(sol.x[:,order[i]]),
                Plots.text(L"\times10^{-3}", 6, :black, :center))])
        end
    end
    p = plot(sol.t, sol.λ[:, end], color=c[end], label=L"\lambda_{d\cdot c}(t)")
    xlims!(time_bounds(sol))
    ylims!(0, 1.05 * maximum(sol.λ[:,end]))
    push!(ps, p)
    plot(ps..., layout=(2, 3), size=(500, 300))
end

function plot_control(sol::DOCPSolution; mode::Symbol=:split)
    if mode == :group
        p1 = plot(sol.t, sol.u[:,1], label=latexify(u_lab[1]))
        plot!(twinx(), sol.t, sol.u[:,2], label=latexify(u_lab[2]), color=:red)
    elseif mode == :split
        p1 = plot(sol.t, sol.u[:,1], label=latexify(u_lab[1]))
        p2 = plot(sol.t, sol.u[:,2], label=latexify(u_lab[2]), color=:red)
        plot(p1, p2, layout=(2, 1), size=(500, 300))
    end
    xlims!(time_bounds(sol))
end

export PLOTS_DEFAULTS, plots_defaults
export plot_objective_trajectory, plot_state, plot_costate, plot_control