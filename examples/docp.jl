import Pkg
Pkg.activate(".")
using ABcontrol, Plots, LaTeXStrings

# export predefined parameters
@export_parameters(ab_model)
include("dynamics.jl")
include("hamiltonian.jl")

# Direct optimization of DOCP ----------------------------------------------

# Resolution parameters
x_dim = 6
u_dim = 2
time_steps = 5000
rk = rk_data(:gauss2)
t_range = 0, 20

# Boundary conditions and constraints
x0 = [0.1629, 0.0487, 0.0003, 0.0177, 0.035, 0]
x_lb = [0, 0, 0, qmin, 0, 0]
dmax = 1.5
u_lb = [0, 0]
u_ub = [1, dmax]

# Define and solve DOCP
docp = new_docp(x_dim, u_dim, ab_system, time_steps, t_range, rk;
    objective_sense=:max, state_lb=x_lb, control_lb=u_lb, control_ub=u_ub)

set_initial_state!(docp, x0)

sol = solve!(docp, t_range, time_steps; tol=1e-12)

# Plots

# PLOTS --------------------------------------------------------------------

plots_defaults()

# Singular arcs validation

function plot_singular_alpha(sol::DOCPSolution)
    N = size(sol.x, 1)
    alpha = Vector{Float64}(undef, N)
    for i = 1:N
        alpha[i] = α_singular(sol.x[i, :], sol.λ[i, :], sol.u[i, 2])
    end
    plot(sol.t, sol.u[:, 1], label=L"\alpha_{\mathrm{direct}}(t)")
    plot!(sol.t, alpha, label=L"\alpha_{\mathrm{singular}}(t)")
    xlims!(time_bounds(sol))
    ylims!(-1, 2.5)
end

function plot_singular_d(sol::DOCPSolution)
    N = size(sol.x, 1)
    d = Vector{Float64}(undef, N)
    for i = 1:N
        d[i] = d_singular(sol.x[i, :], sol.λ[i, :], 1, sol.u[i, 1])
    end
    plot(sol.t, sol.u[:, 2], label=L"d_{\mathrm{direct}}(t)")
    plot!(sol.t, d, label=L"d_{\mathrm{singular}}(t)")
    xlims!(time_bounds(sol))
    ylims!(-0.01, 1.51)
end

function plot_singular_arcs(sol::DOCPSolution)
    p1 = plot_singular_alpha(sol)
    p2 = plot_singular_d(sol)
    plot(p1, p2, layout=(2, 1))
end

function plot_alpha(sol::DOCPSolution)
    N = size(sol.x, 1)
    alpha = [α_singular(sol.x[i,:], sol.λ[i,:], sol.u[i,2]) for i=1:N]
    p1 = plot(sol.t, alpha, label=L"\alpha_{\mathrm{singular}}(t)")
    plot!(sol.t, sol.u[:,1], label=L"\alpha_{\mathrm{direct}}(t)")
    ylabel!(L"\alpha(t)")
    ylims!(-1, 2.5)
    switch = [switch_alpha(sol.x[i,:], sol.λ[i,:]) for i=1:N]
    p2 = plot(sol.t, switch, legend=false)
    ylabel!(L"\zeta_\alpha(x,\lambda)")
    ylims!(-0.02, 0.08)
    plot(p1, p2, layout=(2, 1))
    xlims!(time_bounds(sol))
end

function plot_d(sol::DOCPSolution)
    N = size(sol.x, 1)
    d = [d_singular(sol.x[i,:], sol.λ[i,:], 1, sol.u[i,1]) for i=1:N]
    p1 = plot(sol.t, d, label=L"d_{\mathrm{singular}}(t)")
    plot!(sol.t, sol.u[:,2], label=L"d_{\mathrm{direct}}(t)")
    ylabel!(L"d(t)")
    ylims!(-0.01, 1.51)
    switch = [switch_d(sol.x[i,:], sol.λ[i,:], 1) for i=1:N]
    p2 = plot(sol.t, switch, legend=false)
    ylabel!(L"\zeta_d(x,\lambda, \lambda_0)")
    ylims!(switch[1], switch[end])
    plot(p1, p2, layout=(2, 1))
    xlims!(time_bounds(sol))
end

# plot and save figures

function save_plots(sol::DOCPSolution, time_steps::Integer; path::String="plots")
    if path[end] != '/'
        path *= "/"
    end
    mkpath(path)
    fname(s) = path * "docp_" * string(time_steps) * "_" * s * ".pdf"

    savefig(plot_state(sol), fname("state"))
    savefig(plot_costate(sol), fname("costate"))
    savefig(plot_control(sol), fname("control"))
    savefig(plot_singular_arcs(sol), fname("singular"))
    savefig(plot_alpha(sol), fname("alpha"))
    savefig(plot_d(sol), fname("d"))
end

save_plots(sol, time_steps, path="plots/")

zeta_a = [switch_alpha(sol.x[i,:], sol.λ[i,:]) for i=1:length(sol.t)]
zeta_d = [switch_d(sol.x[i,:], sol.λ[i,:], 1) for i=1:length(sol.t)]
a_sing = [α_singular(sol.x[i,:], sol.λ[i,:], sol.u[i,2]) for i=1:length(sol.t)]
d_sing = [d_singular(sol.x[i,:], sol.λ[i,:], 1, sol.u[i,1]) for i=1:length(sol.t)]
write_docp(sol, switch=[zeta_a zeta_d], singular=[a_sing d_sing])