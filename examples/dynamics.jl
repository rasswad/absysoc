
# System dynamics
function ab_system(x, u)
    s, e, v, q, c = x
    α, d = u
    return [
        d*(s_in - s) - ϕ(s)*e/γ,    # s
        ((1 - α)*ϕ(s) - d) * e,     # e
        α*β*ϕ(s)*e - ρ(v)*c - d*v,  # v
        ρ(v) - μ(q)*q,              # q
        (μ(q) - d) * c,             # c
        d * c                       # objective
    ]
end

# REACTION RATES ------------------------------------------------

# functions
ϕ(s) = ϕmax * s / (ks + s)
ρ(v) = ρmax * v / (kv + v)
μ(q) = μmax * (1 - qmin / q)

# Reaction rate functions derivatives
d_ϕ(s) = ϕmax * ks / (ks + s)^2
d_ρ(v) = ρmax * kv / (kv + v)^2
d_μ(q) = μmax * qmin / q^2

d2_ϕ(s) = -2 * ϕmax * ks / (ks + s)^3
d2_ρ(v) = -2 * ρmax * kv / (kv + v)^3
d2_μ(q) = -2 * μmax * qmin / q^3

# inverse functions
ϕinv(y) = ks * y / (ϕmax - y)
ρinv(y) = kv * y / (ρmax - y)
μinv(y) = qmin * μmax / (μmax - y)