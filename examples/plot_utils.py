import numpy as np
import matplotlib as mpl
from matplotlib.gridspec import GridSpec
from matplotlib import pyplot as plt


# GLOBAL PLOT SETTINGS ################################

TEXT_WIDTH = 7      # \textwidth
COL_WIDTH = 3.4     # \columnwidth
WIDTH = TEXT_WIDTH

def set_mpl_global(width='text'):
    if width.lower() in ['col', 'column']:
        width = COL_WIDTH
        ft_size = 10
    else:
        width = TEXT_WIDTH
        ft_size = 14
    c = ['0C5DA5', '00B945', 'FF9500', 'FF2C00', '845B97', '474747', '9e9e9e']
    mpl.rcParams.update({
        'text.usetex': True,
        'text.latex.preamble': r'\usepackage{bm}',
        'mathtext.default': 'regular',
        "font.family": "serif",
        "mathtext.fontset": "cm",
        "figure.figsize": [width, 0.6 * width],
        "font.size": ft_size,
        "pdf.fonttype": 42,
        "axes.prop_cycle": mpl.cycler('color', c),
        "xtick.direction": "in",
        "ytick.direction": "in",
        "xtick.labelsize": "small",
        "ytick.labelsize": "small",
        "axes.linewidth": 0.5,
        "axes.formatter.use_mathtext": True,
        "lines.linewidth": 1.5,
        "savefig.dpi": 600,
    })

LEGEND_KW = dict(fancybox=False, edgecolor="black", facecolor="white",
                 framealpha=1)

# better legend function
def legend(*axs, **kwargs):
    # kwargs overrides LEGEND_KW
    # equivalent to the script in Python >= 3.9
    # kwargs = LEGEND_KW | kwargs
    kwargs = {**LEGEND_KW, **kwargs}
    if len(axs) == 0:
        leg = plt.gca().legend(**kwargs)
    elif len(axs) == 1:
        leg = axs[0].legend(**kwargs)
    else:
        all_lines = []
        all_labels = []
        for ax in axs:
            lines, labels = ax.get_legend_handles_labels()
            all_lines += lines
            all_labels += labels
        leg = axs[0].legend(all_lines, all_labels, **kwargs)
    leg.get_frame().set_linewidth(0.5)
    return leg


# PLOTTING FUNCTIONS ##################################

# state trajectories
def plot_state(t, x, x_dotted=None, path=''):
    t_range = t[0], t[-1]

    fig = plt.figure(figsize=(WIDTH, 0.45 * WIDTH))
    gs = GridSpec(2, 6, figure=fig, hspace=0.4, wspace=1,
                  top=0.92, bottom=0.07, left=0.06, right=0.97)


    c = mpl.cycler(color=['b', 'orange', 'cyan', 'magenta', 'lime'])
    mpl.rcParams.update({'axes.prop_cycle': c})

    ax11 = fig.add_subplot(gs[0, :2])
    ax12 = fig.add_subplot(gs[0, 2:4])
    ax13 = fig.add_subplot(gs[0, 4:])
    ax21 = fig.add_subplot(gs[1, 1:3])
    ax22 = fig.add_subplot(gs[1, 3:5])
    axs = [ax11, ax12, ax13, ax21, ax22]
    # dynamic solution
    ax11.plot(t, x[0], color='C0', label='$s(t)$')
    ax12.plot(t, x[2], color='C1', label='$v(t)$')
    ax13.plot(t, x[3], color='C2', label='$q(t)$')
    ax21.plot(t, x[1], color='C3', label='$e(t)$')
    ax22.plot(t, x[4], color='C4', label='$c(t)$')
    # dotted solution
    if x_dotted is not None:
        ax11.plot(t, x_dotted[0], '--', color='C0', label='$s(t)$')
        ax12.plot(t, x_dotted[2], '--', color='C1', label='$v(t)$')
        ax13.plot(t, x_dotted[3], '--', color='C2', label='$q(t)$')
        ax21.plot(t, x_dotted[1], '--', color='C3', label='$e(t)$')
        ax22.plot(t, x_dotted[4], '--', color='C4', label='$c(t)$')
    # labels
    ax11.annotate("$s(t)$", (0.4, 0.6), fontsize=20, xycoords='axes fraction')
    ax12.annotate("$v(t)$", (0.4, 0.6), fontsize=20, xycoords='axes fraction')
    ax13.annotate("$q(t)$", (0.4, 0.6), fontsize=20, xycoords='axes fraction')
    ax21.annotate("$e(t)$", (0.4, 0.2), fontsize=20, xycoords='axes fraction')
    ax22.annotate("$c(t)$", (0.4, 0.2), fontsize=20, xycoords='axes fraction')

    for ax in [ax12, ax13, ax21]:
        ax.ticklabel_format(axis='y', scilimits=(-3, -3))
    for ax in axs:
        ax.set_xlim(t_range)
        ax.set_ylim(0, ax.get_ylim()[1])
        ax.grid(visible=True, alpha=0.3)
    
    if path:
        plt.savefig(path)

# costate trajectories
def plot_costate(t, p, path=''):
    t_range = t[0], t[-1]

    fig = plt.figure(figsize=(WIDTH, 0.35 * WIDTH))
    gs = GridSpec(2, 6, figure=fig, hspace=0.3, wspace=1,
                  top=0.95, bottom=0.1, left=0.06, right=0.97)


    c = mpl.cycler(color=['b', 'orange', 'cyan', 'magenta', 'lime'])
    mpl.rcParams.update({'axes.prop_cycle': c})

    ax11 = fig.add_subplot(gs[0, :2])
    ax12 = fig.add_subplot(gs[0, 2:4])
    ax13 = fig.add_subplot(gs[0, 4:])
    ax21 = fig.add_subplot(gs[1, 1:3])
    ax22 = fig.add_subplot(gs[1, 3:5])
    axs = [ax11, ax12, ax13, ax21, ax22]
    # dynamic solution
    ax11.plot(t, p[0], color='C0', label='$s(t)$')
    ax12.plot(t, p[2], color='C1', label='$v(t)$')
    ax13.plot(t, p[3], color='C2', label='$q(t)$')
    ax21.plot(t, p[1], color='C3', label='$e(t)$')
    ax22.plot(t, p[4], color='C4', label='$c(t)$')
    # labels
    ax11.annotate("$\\lambda_s(t)$", (0.4, 0.25), fontsize=20, xycoords='axes fraction')
    ax12.annotate("$\\lambda_v(t)$", (0.4, 0.25), fontsize=20, xycoords='axes fraction')
    ax13.annotate("$\\lambda_q(t)$", (0.4, 0.25), fontsize=20, xycoords='axes fraction')
    ax21.annotate("$\\lambda_e(t)$", (0.4, 0.25), fontsize=20, xycoords='axes fraction')
    ax22.annotate("$\\lambda_c(t)$", (0.4, 0.3), fontsize=20, xycoords='axes fraction')

    for ax in axs:
        ax.set_xlim(t_range)
        ax.set_ylim(0, ax.get_ylim()[1])
        ax.grid(visible=True, alpha=0.3)
    
    if path:
        plt.savefig(path)

# alpha control (direct + singular) and switch
def plot_alpha(t, u, u_sing, switch, path=''):
    mpl.rcParams.update({
        "axes.prop_cycle": mpl.rcParamsDefault["axes.prop_cycle"],
    })

    fig, (con, zet) = plt.subplots(2, 1, sharex=True)
    fig.set_size_inches(WIDTH, 0.4 * WIDTH)
    con.plot(t, u_sing[0], 'C1', label=r"$\alpha_{\mathrm{singular}}(t)$")
    con.plot(t, u[0], 'C0', label=r"$\alpha_{\mathrm{direct}}(t)$")
    zet.plot(t, switch[0], 'C3', label=r"$\zeta_{\alpha}(t)$")

    con.annotate(r"$\alpha_{\mathrm{direct}}(t)$", (1.3, 0.2), color='C0')
    con.annotate(r"$\alpha_{\mathrm{singular}}(t)$", (1.3, -0.6), color='C1')

    zet.set_xlabel("Time [days]")
    con.set_ylabel("$\\alpha(t)$")
    zet.set_ylabel(r"$\zeta_{\alpha}(\mathbf{x},\bm{\lambda})$")
    fig.align_ylabels()
    zet.set_xlim(t[0], t[-1])
    con.set_ylim(-1, 2.4)
    zet.set_ylim(-0.02, 0.07)

    for ax in [con, zet]:
        ax.grid(visible=True, alpha=0.3)

    fig.subplots_adjust(
        top=0.96,
        bottom=0.22,
        left=0.11,
        right=0.97,
        hspace=0.2
    )

    if path:
        plt.savefig(path)

# d control (direct) and switch (subplots)
def plot_d(t, u, u_sing, switch, path=''):
    mpl.rcParams.update({
        "axes.prop_cycle": mpl.rcParamsDefault["axes.prop_cycle"],
    })

    fig, (con, zet) = plt.subplots(2, 1, sharex=True)
    fig.set_size_inches(WIDTH, 0.4 * WIDTH)
    for ax in [con, zet]:
        ax.grid(visible=True, alpha=0.3)
        ax.axvline(x=1.36, ymin=0, ymax=1, color='k', ls='--')
        ax.axvline(x=18.652, ymin=0, ymax=1, color='k', ls='--')
    
    con.plot(t, u_sing[1], 'C1', label=r"$d_{\mathrm{singular}}(t)$")
    con.plot(t, u[1], 'C0', label=r"$d_{\mathrm{direct}}(t)$")
    zet.plot(t, switch[1], 'C3', label=r"$\zeta_d(t)$")

    zet.annotate("$t_1^d$", (1.6, 0.08), color='k')
    zet.annotate("$t_2^d$", (17.7, 0.08), color='k')
    con.annotate(r"$d_{\mathrm{direct}}(t)$",   (1.8, 1.2), color='C0')
    con.annotate(r"$d_{\mathrm{singular}}(t)$", (1.8, 0.6), color='C1')

    zet.set_xlabel("Time [days]")
    con.set_ylabel("$d(t)$")
    zet.set_ylabel(r"$\zeta_d(\mathbf{x},\bm{\lambda},\lambda_0)$")
    fig.align_ylabels()
    con.set_ylim(-0.3, 1.8)
    zet.set_xlim(t[0], t[-1])


    fig.subplots_adjust(
        top=0.96,
        bottom=0.22,
        left=0.11,
        right=0.97,
        hspace=0.2
    )
    
    if path:
        plt.savefig(path)

# direct controls with socp comparison
def plot_control(t, u, u_socp, path=''):
    mpl.rcParams.update({
        "axes.prop_cycle": mpl.rcParamsDefault["axes.prop_cycle"],
    })

    fig, axs = plt.subplots(2, 1, sharex=True)
    fig.set_size_inches(WIDTH, 0.5 * WIDTH)
    ax1, ax2 = axs
    
    c_opt = "C0"
    c_cst = "C1"
    c_avg = "C2"

    u_avg = u.mean(axis=1)
    
    # alpha control
    ax1.plot(t, u[0], c_opt, label=r"$\alpha_{\mathrm{direct}}(t)$", zorder=10)
    ax1.axhline(y=u_socp[0], xmin=0, xmax=1, color=c_cst, label=r"$\overline{\alpha}$")
    ax1.axhline(y=u_avg[0], xmin=0, xmax=1, color=c_avg, ls='dashed', label=r"$\alpha_{\mathrm{average}}$")
    #ax1.annotate(r"$\overline{\alpha}$", (2.6, 0.6), color=c_cst)
    #ax1.annotate(r"$\alpha_{\mathrm{average}}$", (2.6, 1.1), color=c_avg)
    ax1.set_ylabel("$\\alpha(t)$")
    ax1.set_ylim(-0.5, 1.5)
    legend_kw = dict(fontsize=12, handlelength=1.6, handletextpad=0.5)
    legend(ax1, loc="lower left", **legend_kw)
    
    # d control
    ax2.plot(t, u[1], c_opt, label=r"$d_{\mathrm{direct}}(t)$", zorder=10)
    ax2.axhline(y=u_socp[1], xmin=0, xmax=1, color=c_cst, label=r"$\overline{d}$")
    ax2.axhline(y=u_avg[1], xmin=0, xmax=1, color=c_avg, ls='dashed', label=r"$d_{\mathrm{average}}$")
    #ax2.annotate(r"$\overline{d}$", (2.6, u_socp[1]+0.1), color=c_cst)
    #ax2.annotate(r"$d_{\mathrm{average}}$", (0.6, 0.2), color=c_avg)
    ax2.set_ylabel("$d(t)$")
    ax2.set_ylim(-0.075, 1.575)
    legend(ax2, loc="upper left", **legend_kw)
    
    # common configs
    for ax in axs:
        ax.grid(visible=True, alpha=0.3)

    # common axis
    ax2.set_xlabel("Time [days]")
    ax2.set_xlim(t[0], t[-1])
    fig.align_ylabels()

    fig.tight_layout()
    fig.subplots_adjust(
        top=0.98,
        bottom=0.14,
        left=0.095,
        right=0.96,
        hspace=0.1
    )

    if path:
        plt.savefig(path)


# dswitch
def plot_control2(t, u, u_socp, d_switch, d_max, path=''):
    mpl.rcParams.update({
        "axes.prop_cycle": mpl.rcParamsDefault["axes.prop_cycle"],
    })

    fig, axs = plt.subplots(2, 1, sharex=True)
    fig.set_size_inches(WIDTH, 0.5 * WIDTH)
    ax1, ax2 = axs
    
    c_opt = "C0"
    c_cst = "C1"
    c_avg = "C2"

    u_avg = u.mean(axis=1)
    
    # alpha control
    ax1.plot(t, u[0], c_opt, label=r"$\alpha_{\mathrm{direct}}(t)$", zorder=10)
    ax1.axhline(y=u_socp[0], xmin=0, xmax=1, color=c_cst, label=r"$\overline{\alpha}$")
    ax1.axhline(y=u_avg[0], xmin=0, xmax=1, color=c_avg, ls='dashed', label=r"$\alpha_{\mathrm{average}}$")
    #ax1.annotate(r"$\overline{\alpha}$", (2.6, 0.6), color=c_cst)
    #ax1.annotate(r"$\alpha_{\mathrm{average}}$", (2.6, 1.1), color=c_avg)
    ax1.set_ylabel("$\\alpha(t)$")
    ax1.set_ylim(-0.5, 1.5)
    legend_kw = dict(fontsize=12, handlelength=1.6, handletextpad=0.5)
    legend(ax1, loc="lower left", **legend_kw)
    
    # d control
    t_socp = np.array([t[0], t[d_switch], t[d_switch], t[-1]])
    d_socp = np.array([u_socp[1], u_socp[1], d_max, d_max])
    ax2.plot(t, u[1], c_opt, label=r"$d_{\mathrm{direct}}(t)$", zorder=10)
    #ax2.axhline(y=u_socp[1], xmin=0, xmax=1, color=c_cst, label=r"$\overline{d}$")
    ax2.plot(t_socp, d_socp, color=c_cst, label=r"$\overline{d}$")
    ax2.axhline(y=u_avg[1], xmin=0, xmax=1, color=c_avg, ls='dashed', label=r"$d_{\mathrm{average}}$")
    #ax2.annotate(r"$\overline{d}$", (2.6, u_socp[1]+0.1), color=c_cst)
    #ax2.annotate(r"$d_{\mathrm{average}}$", (0.6, 0.2), color=c_avg)
    ax2.set_ylabel("$d(t)$")
    ax2.set_ylim(-0.075, 1.575)
    legend(ax2, loc="upper left", **legend_kw)
    
    # common configs
    for ax in axs:
        ax.grid(visible=True, alpha=0.3)

    # common axis
    ax2.set_xlabel("Time [days]")
    ax2.set_xlim(t[0], t[-1])
    fig.align_ylabels()

    fig.tight_layout()
    fig.subplots_adjust(
        top=0.98,
        bottom=0.14,
        left=0.095,
        right=0.96,
        hspace=0.1
    )

    if path:
        plt.savefig(path)

def plot_one_control(t, u, u_socp, control, d_switch, d_max, path=''):
    mpl.rcParams.update({
        "axes.prop_cycle": mpl.rcParamsDefault["axes.prop_cycle"],
    })

    fig, axs = plt.subplots(2, 1, sharex=True)
    fig.set_size_inches(WIDTH, 0.5 * WIDTH)
    ax1, ax2 = axs
    
    c_opt = "C0"
    c_cst = "C1"

    if control.lower() == "alpha":
        # alpha control
        ax1.plot(t, u, c_opt, label=r"$\alpha_{\mathrm{direct}}(t)$", zorder=10)
        ax1.axhline(y=u_socp[0], xmin=0, xmax=1, color=c_cst, label=r"$\overline{\alpha}$")
        ax1.set_ylabel("$\\alpha(t)$")
        ax1.set_ylim(-0.5, 1.5)
        legend_kw = dict(fontsize=12, handlelength=1.6, handletextpad=0.5)
        legend(ax1, loc="lower left", **legend_kw)
        
        # d control
        ax2.axhline(y=u_socp[1], xmin=0, xmax=1, color=c_cst, label=r"$\overline{d}$")
        ax2.set_ylabel("$d(t)$")
        ax2.set_ylim(-0.075, 1.575)
        legend(ax2, loc="upper left", **legend_kw)
    elif control.lower() == "d":
        # alpha control
        ax1.axhline(y=u_socp[0], xmin=0, xmax=1, color=c_cst, label=r"$\overline{\alpha}$")
        ax1.set_ylabel("$\\alpha(t)$")
        ax1.set_ylim(-0.5, 1.5)
        legend_kw = dict(fontsize=12, handlelength=1.6, handletextpad=0.5)
        legend(ax1, loc="lower left", **legend_kw)
        
        # d control
        t_socp = np.array([t[0], t[d_switch], t[d_switch], t[-1]])
        d_socp = np.array([u_socp[1], u_socp[1], d_max, d_max])
        ax2.plot(t, u, c_opt, label=r"$d_{\mathrm{direct}}(t)$", zorder=10)
        #ax2.axhline(y=u_socp[1], xmin=0, xmax=1, color=c_cst, label=r"$\overline{d}$")
        ax2.plot(t_socp, d_socp, color=c_cst, label=r"$\overline{d}$")
        ax2.set_ylabel("$d(t)$")
        ax2.set_ylim(-0.075, 1.575)
        legend(ax2, loc="upper left", **legend_kw)
    
    # common configs
    for ax in axs:
        ax.grid(visible=True, alpha=0.3)

    # common axis
    ax2.set_xlabel("Time [days]")
    ax2.set_xlim(t[0], t[-1])
    fig.align_ylabels()

    fig.tight_layout()
    fig.subplots_adjust(
        top=0.98,
        bottom=0.14,
        left=0.095,
        right=0.96,
        hspace=0.1
    )

    if path:
        plt.savefig(path)

# direct controls and switch (twin axes on 2 subplots)
def plot_control_cdc(t, u, u_socp, switch, path=''):
    mpl.rcParams.update({
        'text.usetex': True,
        'mathtext.default': 'regular',
        'text.latex.preamble': r'\usepackage{bm}',
        "font.size": 10,
        "lines.linewidth": 1.0,
    })

    fig, axs = plt.subplots(2, 1, sharex=True)
    WIDTH = 3.4
    fig.set_size_inches(WIDTH, 0.6 * WIDTH)
    ax11, ax21 = axs
    ax12 = ax11.twinx()
    ax22 = ax21.twinx()

    c_opt = "tab:red"
    c_cst = "tab:green"
    c_zet = "tab:blue"

    # alpha control
    ax11.axhline(y=u_socp[0], xmin=0, xmax=1, color=c_cst, ls='dashed')
    ax11.annotate(r"$\overline{\alpha}$", (2.5, 0.5), color=c_cst)
    ax12.plot(t, switch[0], c_zet)
    ax11.plot(t, u[0], c_opt)
    ax11.set_ylabel("$\\alpha(t)$")
    ax12.set_ylabel(r"$\zeta_{\alpha}(\mathbf{x},\bm{\lambda})$")
    ax11.set_ylim(-0.5, 1.5)
    ax12.set_ylim(-0.01, 0.03)
    ax11.tick_params(axis='y', labelcolor=c_opt)
    ax12.tick_params(axis='y', labelcolor=c_zet)
    ax11.yaxis.get_label().set_color(c_opt)
    ax12.yaxis.get_label().set_color(c_zet)
    
    # d control
    ax21.axvline(x=1.36, ymin=0, ymax=1, color='k', ls='--')
    ax21.axvline(x=18.652, ymin=0, ymax=1, color='k', ls='--')
    ax21.axhline(y=u_socp[1], xmin=0, xmax=1, color=c_cst, ls='dashed')
    ax21.annotate(r"$\overline{d}$", (2.5, u_socp[1]+0.1), color=c_cst)
    ax21.plot(t, u[1], c_opt)
    ax22.plot(t, switch[1], c_zet)
    ax21.annotate("$t_1^d$", (1.6, 1), color='k')
    ax21.annotate("$t_2^d$", (17.5, 1), color='k')
    ax21.set_ylabel("$d(t)$")
    ax22.set_ylabel(r"$\zeta_d(\mathbf{x},\bm{\lambda},\lambda_0)$")
    ax21.set_ylim(-0.5, 1.5)
    ax22.set_ylim(-0.07, 0.21)
    ax21.tick_params(axis='y', labelcolor=c_opt)
    ax22.tick_params(axis='y', labelcolor=c_zet)
    ax21.yaxis.get_label().set_color(c_opt)
    ax22.yaxis.get_label().set_color(c_zet)
    
    # common configs
    for ax in axs:
        ax.grid(visible=True, alpha=0.3)

    # common axis
    ax21.set_xlabel("Time [days]")
    ax21.set_xlim(t[0], t[-1])
    fig.align_ylabels()

    fig.tight_layout()
    fig.subplots_adjust(
        top=0.99,
        bottom=0.175,
        left=0.095,
        right=0.86,
        hspace=0.07
    )

    if path:
        plt.savefig(path)
    
# socp objective contours
def plot_objective(alpha, d, obj, d_washout, u_max, path=''): 
    mpl.rcParams.update({
        "axes.prop_cycle": mpl.rcParamsDefault["axes.prop_cycle"],
        "font.size": 10,
    })

    fig = plt.figure(figsize=(COL_WIDTH, 0.6*COL_WIDTH))
    ax = fig.add_subplot()
    cmap = plt.cm.magma
    bg = mpl.colors.to_hex(cmap(0))
    ax.set_facecolor(bg)

    # contours
    A, D = np.meshgrid(alpha, d)
    cs = ax.contourf(A, D, obj, 25, cmap=cmap)
    cb = fig.colorbar(cs)
    ax.set_xlabel(r"Optogenetic control $\alpha$")
    ax.set_ylabel("Dilution rate $d$")
    cb.set_label(r"$f_0^*(\alpha,d)$")#~[g.L^{-1}.\mathrm{day}^{-1}]$")
    #cb.ax.ticklabel_format(scilimits=(-3, -3))
    cb.ax.yaxis.set_major_locator(mpl.ticker.MultipleLocator(0.08))
    ax.yaxis.set_major_locator(mpl.ticker.MultipleLocator(0.2))

    # washout
    ax.plot(alpha, d_washout, 'C3', label='washout', zorder=50)
    ax.plot(u_max[0], u_max[1], '.', color='C3', lw=3, label="maximum")

    ax.set_xlim(0, 1)
    legend(fig, ncols=2, loc='outside upper left', bbox_to_anchor=(0.115, 1.02))
    fig.tight_layout()
    fig.subplots_adjust(
        top=0.84,
        bottom=0.18,
        left=0.135,
        right=0.97
    )
    if path:
        plt.savefig(path)

