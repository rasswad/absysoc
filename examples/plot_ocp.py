import numpy as np

from examples.plot_utils import *


def load_array(path, silent=True):
    try:
        return np.loadtxt(path).T
    except FileNotFoundError:
        if not silent:
            print(f"File '{path}' not found, skipping.")
        return None
    return None


def plot_ocp_dir(path):
    t = load_array(f"{path}/docp_time.array")
    docp_x = load_array(f"{path}/docp_state.array")
    socp_x = load_array(f"{path}/socp_trajectory.array")
    docp_u = load_array(f"{path}/docp_control.array")
    socp_u = load_array(f"{path}/socp_control.array")
    #x_eq = load_array(f"{path}/equilibrium.array")

    if docp_x is not None and socp_x is not None:
        plot_state(t, docp_x, x_dotted=socp_x, path=f"{path}/state_trajectory.pdf")
    if docp_u is not None and socp_u is not None:
        plot_control(t, docp_u, socp_u, path=f"{path}/control_trajectory.pdf")


if __name__ == '__main__':
    import argparse, os

    parser = argparse.ArgumentParser(description='Plot OCP solution(s).')
    parser.add_argument('dir_path', type=str, help='The path to the results directory.')
    parser.add_argument('-p', '--parent-dir', action='store_true', default=False,
                        help='plot solutions in subdirectories.')
    
    args = parser.parse_args()
    path = args.dir_path
    if path[-1] == '/': path = path[:-1]

    if args.parent_dir:
        for file in os.scandir(path):
            sub_path = f"{path}/{file.name}"
            if os.path.isdir(sub_path):
                plot_ocp_dir(sub_path)
                plt.close('all')    # saves memory
    else:
        plot_ocp_dir(path)
        plt.show()
