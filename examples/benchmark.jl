import Pkg
Pkg.activate(".")
using ABcontrol, JuMP, DifferentialEquations
using DelimitedFiles, Suppressor, YAML, CSV, DataFrames, Dates

path = "results/benchmark"
data_path = path * "/" * "data.csv"
function save(id, fname)
    subpath = path * "/" * string(id)
    mkpath(subpath)
    return subpath * "/" * fname
end

# ------------------------------------------------------------------

# System parameters
β = 23e-3
γ = 0.44
# Reaction rate parameters
ϕmax = 6.48; ks = 0.09
ρmax = 27.3e-3; kv = 0.57e-3
μmax = 1.0211; qmin = 2.7628e-3
# Reaction rate functions
ϕ(s) = ϕmax * s / (ks + s)
ρ(v) = ρmax * v / (kv + v)
μ(q) = μmax * (1 - qmin / q)

# System dynamics with varying s_in
function ab_system(x, u, s_in)
    s, e, v, q, c = x
    α, d = u
    return [
        d*(s_in - s) - ϕ(s)*e/γ,    # s
        ((1 - α)*ϕ(s) - d) * e,     # e
        α*β*ϕ(s)*e - ρ(v)*c - d*v,  # v
        ρ(v) - μ(q)*q,              # q
        (μ(q) - d) * c,             # c
        d * c                       # objective
    ]
end

# ------------------------------------------------------------------

# Get optimal constant u=(α, d)' at equilibrium state
function solve_socp(s_in; tol=1e-14, print_level=4, log_path=nothing)
    socp = new_socp(2, [0, 0], [1, Inf])

    # expressions at steady state
    a = socp.obj_dict[:u][1]
    d = socp.obj_dict[:u][2]
    s = ϕinv(d / (1 - a))
    e = (1 - a) * γ * (s_in - s) # not needed for socp
    v_in = a * β * γ * (s_in - s)
    q = μinv(d)
    v = ρinv(d * q)
    c = (v_in - v) / q

    # nonlinear constraints
    @constraints(socp, begin
        d / (1 - a) <= ϕmax  # ϕ inverse domain
        s_in >= s            # v_in ≥ 0
        d <= μmax            # μ inverse domain
        d * q <= ρmax        # ρ inverse domain
        v_in >= v            # c ≥ 0
    end)

    @objective(socp, Max, d * c)

    # debug
    sol = ABcontrol.solve!(socp; tol=tol, print_level=print_level,
        max_iter=5000, log_path=log_path)
    x_eq = value.([s, e, v, q, c, sol.obj])
    return sol, x_eq
end

# get `i` s.t. `d[i]=dmax` and `d[j]<dmax` for all `j<i`
function max_dilution_time(d; dmax=1.5, epsilon=1e-5)
    for i = length(d)-1:-1:1
        if abs(d[i] - dmax) >= epsilon
            return i
        end
    end
    return length(d)
end

# ------------------------------------------------------------------

# solve docp
function initialize_docp(s_in, x0, tf, time_steps, control="both", socp_u=nothing)
    x_dim = 6
    t_range = 0, tf
    x_lb = [0, 0, 0, qmin, 0, 0]
    x0[end] = 0.0
    rk = rk_data(:gauss2)
    dmax = 1.5
    if isnothing(socp_u) || control == "both"
        u_dim = 2
        f_docp(x, u) = ab_system(x, u, s_in)
        u_lb = [0, 0]
        u_ub = [1, dmax]
        docp = new_docp(x_dim, u_dim, f_docp, time_steps, t_range, rk;
            objective_sense=:max, state_lb=x_lb, control_lb=u_lb, control_ub=u_ub)
        set_initial_state!(docp, x0)
        return docp
    end
    u_dim = 1
    u_lb = [0]
    if control == "alpha"
        u_ub = [1]
        f_docp_a(x, u) = ab_system(x, [u[1], socp_u[2]], s_in)
        docp = new_docp(x_dim, u_dim, f_docp_a, time_steps, t_range, rk;
            objective_sense=:max, state_lb=x_lb, control_lb=u_lb, control_ub=u_ub)
        set_initial_state!(docp, x0)
        return docp
    end
    if control == "d"
        u_ub = [dmax]
        f_docp_d(x, u) = ab_system(x, [socp_u[1], u[1]], s_in)
        docp = new_docp(x_dim, u_dim, f_docp_d, time_steps, t_range, rk;
            objective_sense=:max, state_lb=x_lb, control_lb=u_lb, control_ub=u_ub)
        set_initial_state!(docp, x0)
    end
    @assert false "unknown control" * control
    return nothing
end

# set initial guess from socp
function set_initial_from_socp!(docp::GenericModel, s_in, t_range, x0, u; tol=1e-10)
    dt = (t_range[2] - t_range[1]) / (size(docp.obj_dict[:x], 1) - 1)
    f_socp(x, u, t) = ab_system(x, u, s_in)
    x0_ = copy(x0)
    x0_[end] = 0.0
    ode = ODEProblem(f_socp, x0_, t_range, u)
    traj = DifferentialEquations.solve(ode, reltol=tol, saveat=dt)
    x = mapreduce(permutedims, vcat, traj.u)
    set_start_value.(docp.obj_dict[:x], x)
    #for i in 1:size(docp.obj_dict[:u], 2)
    #    set_start_value.(docp.obj_dict[:u][:, i], u[i])
    #end
end

# calculate socp trajectory
function socp_trajectory(s_in, docp_sol, socp_u, control="both"; tol=1e-10)
    t_range = docp_sol.t[1], docp_sol.t[end]
    x0 = Vector{Float64}(undef, size(docp_sol.x, 2))
    x0[:] = docp_sol.x[1, :]
    x0[end] = 0.0
    f_socp(x, u, t) = ab_system(x, u, s_in)
    ode = ODEProblem(f_socp, x0, t_range, socp_u)
    traj = DifferentialEquations.solve(ode, reltol=tol, saveat=step(docp_sol.t))
    x = mapreduce(permutedims, vcat, traj.u)

    if control == "alpha"
        return x, x
    end

    # set maximum dilution for final stretch
    dmax = 1.5
    d_ind = control == "both" ? 2 : 1
    ind_dmax = max_dilution_time(docp_sol.u[:, d_ind]; dmax=dmax)
    x0[:] = docp_sol.x[ind_dmax, :]
    t0 = Float64(docp_sol.t[ind_dmax])
    umax = [socp_u[1], dmax]
    ode = ODEProblem(f_socp, x0, (t0, t_range[2]), umax)
    traj2 = DifferentialEquations.solve(ode, reltol=tol, saveat=step(docp_sol.t))
    x2 = mapreduce(permutedims, vcat, traj2.u)
    new_x = copy(x)
    new_x[ind_dmax:end, :] = x2

    return x, new_x
end

# find best warm start, returns: run, id
# run: boolean to run or skip 
# id: solution id if it exists, otherwise nothing
function get_best_warm_start(s_in, tf, control="both"; x0::Union{Vector{T}, Nothing}=nothing,
        time_steps=1000, tol=1e-10, force=false) where {T<:Real}
    # load benchmark data
    df = CSV.read(path * "/data.csv", DataFrame);
    
    # choose solutions with similar parameters
    filter!(row -> row.control == control && row.s_in == s_in && row.tf == tf, df)
    if isnothing(x0) # if x0 is not given, use equilibrium
        filter!(row -> row.x0 == "eq", df)
    else
        filter!(row -> row.x0 == "rnd", df)
    end

    # sort solutions from best to worst w.r.t. time_steps then tol
    sort!(df, [order(:time_steps, rev=true), :tol])

    # choose solutions that are the same or better
    better = filter(row -> row.time_steps >= time_steps && row.tol <= tol, df)
    if length(better.id) > 0    # similar or better solutions exist
        idem = filter(row -> row.time_steps == time_steps, better)
        if length(idem.id) > 0  # similar solution exists
            println("Solution #" * string(idem.id[1]) * " has similar number of time steps, skipping.")
            return false, idem.id[1]
        elseif force            # force run optimization
            filter!(row -> mod(row.time_steps, time_steps) == 0, better)
            if length(better.id) > 0
                # better solution can be used
                return true, better.id[1]
            else
                # none of the better solutions can be used
                return true, nothing
            end
        end
    end

    # no better solutions exist, find appropriate warm start solution
    filter!(row -> mod(time_steps, row.time_steps) == 0, df)
    if length(df.id) > 0
        # a worse solution can be used
        return true, df.id[1]
    else
        # none of the worse solutions can be used
        return true, nothing
    end
end



# calculate gains
function run_simulation(s_in, tf, control="both";
        x0::Union{Vector{T}, Nothing}=nothing, time_steps=1000, tol=1e-10,
        print_level=4, max_iter=5000,
        docp_sol::Union{DOCPSolution,Nothing}=nothing, force=false) where {T<:Real}
    id = get_new_unique_id(data_path)
    entry = Dict{String,Any}()
    sol = Dict{String,Any}()

    run, warm_start_id = get_best_warm_start(s_in, tf; x0=x0, tol=tol,
        time_steps=time_steps, force=force)
    
    if ~run
        rm(path * "/" * string(id), recursive=true, force=true)
        return nothing, nothing, nothing, true
    end

    # populate entry
    entry["id"] = id
    entry["control"] = control
    entry["s_in"] = s_in
    entry["tf"] = tf

    # Solve and save SOCP
    socp_sol, x_eq = solve_socp(s_in; tol=1e-14, print_level=4, log_path=save(id, "socp.log"))
    if isnothing(x0)
        entry["x0"] = "eq"
        x0 = copy(x_eq)
        x0[end] = 0.0
    else
        entry["x0"] = "rnd"
    end
    writedlm(save(id, "equilibrium.array"), x_eq)
    writedlm(save(id, "socp_control.array"), socp_sol.u)
    sol["socp_sol"] = socp_sol
    sol["eq"] = x_eq

    # Solve and save DOCP
    docp = initialize_docp(s_in, x0, tf, time_steps, control, socp_sol.u)
    t_range = (0, tf)
    if ~isnothing(docp_sol)
        set_initial_guess!(docp, docp_sol)
    elseif ~isnothing(warm_start_id) 
        set_initial_guess!(docp, path * "/" * string(warm_start_id))
        println("Warm start from solution #" * string(warm_start_id))
    else
        set_initial_from_socp!(docp, s_in, t_range, x0, socp_sol.u)
    end
    docp_sol = ABcontrol.solve!(docp, t_range, time_steps; tol=tol,
        print_level=print_level, max_iter=max_iter, log_path=save(id, "docp.log"))
    success = (termination_status(docp) == LOCALLY_SOLVED)

    # if not success, stop here
    if (~success)
        date = Dates.format(now(), dateformat"yyyy-mm-dd_HH-MM")
        fname = path * "/fail/" * date * ".log"
        write(fname, read(save(id, "docp.log"), String))
        open(fname, "a") do io
            println(io, "\nMetadata:")
            println(io, "control = " * control)
            println(io, "s_in = " * string(s_in))
            println(io, "time_steps = " * string(time_steps))
            println(io, "final_time = " * string(tf))
            println(io, "tol = " * string(tol))
            println(io, "max_iter = " * string(max_iter))
            if isnothing(x0)
                println(io, "x0 = eqn")
            else
                println(io, "x0 = rnd")
            end
        end
        println("DOCP did not run successfully, logged at " * fname)
        rm(path * "/" * string(id), recursive=true, force=true)
        return nothing, nothing, nothing, false
    end

    sol["docp_sol"] = docp_sol
    entry["time_steps"] = time_steps
    entry["tol"] = tol
    writedlm(save(id, "time.array"), docp_sol.t)
    writedlm(save(id, "docp_state.array"), docp_sol.x)
    writedlm(save(id, "docp_control.array"), docp_sol.u)

    # Calculate state trajectories for SOCP
    dmax = 1.5
    x1, x2 = socp_trajectory(s_in, docp_sol, socp_sol.u; tol=tol)
    sol["socp_trajectory"] = x2
    entry["raw_gain"] = docp_sol.x[end, end] / x1[end, end] - 1
    entry["adjusted_gain"] = docp_sol.x[end, end] / x2[end, end] - 1
    entry["dmax_index"] = max_dilution_time(docp_sol.u[:, 2]; dmax=dmax)
    if control == "alpha"
        entry["dmax_index"] = nothing
    end
    writedlm(save(id, "socp_state.array"), x2)
    
    entry["date"] = Dates.format(now(), dateformat"yyyy-mm-dd_HH:MM")

    # Write entry in `meta.yml`
    open(save(id, "meta.yml"), "w") do io
        YAML.write(io, entry)
    end

    # Add entry to `data.csv`
    row = [id, s_in, tf, entry["x0"], time_steps, tol, entry["raw_gain"],
        entry["adjusted_gain"], entry["dmax_index"], entry["date"]]
    open(data_path, "a") do io
        println(io, join(row, ","))
    end

    return id, entry, sol, true
end

#---------------

function get_new_unique_id(csv_file::String)
    data = CSV.File(csv_file) 
    ids = [row[1] for row in data]
    return isempty(ids) ? 1 : maximum(ids) + 1
end


#### filter best results

function show_best(x0::String, control::String="both"; time_steps=nothing, tf=40)
    df = CSV.read(path * "/data.csv", DataFrame)
    filter!(row -> row.control == control && row.tf == tf && row.x0 == x0, df) 
    if ~isnothing(time_steps)
        filter!(row -> row.time_steps == time_steps, df)
    end
    best = combine(groupby(df, :s_in)) do subdf
        sort!(subdf, [:time_steps, :tol], rev = [true, false])
        subdf[1, :]
    end
    sort(best, :s_in)
end

#############################################
# BENCHMARK SCRIPT

s_in_vals = cat(
    [0.02, 0.05],
    0.1:0.1:2,
    [2.2, 2.5, 2.8],
    3:0.5:4.5,
    [4.6, 4.7, 4.8, 5, 5.5];
    dims=1
)

x0 = [0.1629, 0.0487, 0.0003, 0.0177, 0.035, 0]
tf = 40
for s_in in s_in_vals
    println(s_in)
    for x0_val in [x0, nothing]
        id, entry, sol, success = run_simulation(s_in, tf; x0=x0_val, tol=1e-10,
            time_steps=5000, print_level=4, max_iter=5000, force=false)
    end
end
