import Pkg
Pkg.activate(".")
using ABcontrol, JuMP, Plots, LaTeXStrings

# export predefined parameters
@export_parameters(ab_model)
include("dynamics.jl")

# Optimization at steady state ---------------------------------------------

# u = (α, d)
# α ∈ [0, 1]
# d ∈ [0, ∞)
socp = new_socp(2, [0, 0], [1, Inf])

# expressions at steady state
a = socp.obj_dict[:u][1]
d = socp.obj_dict[:u][2]
s = ϕinv(d / (1 - a))
v_in = a * β * γ * (s_in - s)
q = μinv(d)
v = ρinv(d * q)
c = (v_in - v) / q

# nonlinear constraints
@constraints(socp, begin
    d / (1 - a) <= ϕmax  # ϕ inverse domain
    s_in >= s            # v_in ≥ 0
    d <= μmax            # μ inverse domain
    d * q <= ρmax        # ρ inverse domain
    v_in >= v            # c ≥ 0
end)

@objective(socp, Max, d * c)

sol = solve!(socp, tol=1e-10, print_level=4)

# Plot SOCP objective and solution -----------------------------------------

function bio_productivity(a, d)
    if d >= d_washout(a)
        return 0
    end
    s = ifelse(d < (1-a)*ϕmax, ϕinv(d / (1 - a)), 0)
    v_in = a * β * γ * max(0, s_in - s)
    q = ifelse(d < μmax, μinv(d), 0)
    v = ifelse(d*q < ρmax, ρinv(d * q), 0)
    c = ifelse(q > 0, max(0, v_in - v) / q, 0)
    return d * c
end

function d_washout(α)
    abg = α * β * γ
    q_mu = qmin * μmax
    rho_qmu = ρmax + q_mu
    rho_mu =  ρmax * μmax
    phi_1_a = (1 - α) * ϕmax
    
    A = abg * rho_qmu * (ks + s_in) + kv * q_mu
    B = abg * (ks + s_in) * rho_mu + phi_1_a * (abg * rho_qmu * s_in + kv * q_mu)
    C = abg * s_in * rho_mu * phi_1_a
    
    delta_rt = sqrt(B^2 - 4 * A * C)
    
    x1 = (B - delta_rt) / (2*A)
    x2 = (B + delta_rt) / (2*A)
    return min(x1, x2)
end

aa = range(0, 1, length=200)
dmax = maximum(d_washout.(aa))
dd = collect(0:0.001:dmax)
obj = @. bio_productivity(aa', dd)
write_socp(sol, alpha=aa, d=dd, d_washout=d_washout.(aa), objective=obj)

plots_defaults()

function plot_objective(alpha_val, d_val, sol::SOCPSolution; log=false)
    obj = @. bio_productivity(alpha_val', d_val)
    if log
        obj = @. ifelse(obj > 0, Base.log(obj), 0)
    end

    p = contourf(alpha_val, d_val, obj, levels=25, color=:magma,
        size=(500, 300), linewidth=0.01, colorbar_title=L"f_0^*(\alpha,d)")
    plot!(alpha_val, d_washout.(alpha_val), color=:red, linewidth=3, label="washout")
    plot!(background_color_inside=:black)
    xlims!(0, 1)
    ylims!(0, 1.1 * d_val[end])
    xlabel!("Optogenetic control " * L"\alpha")
    ylabel!("Dilution rate " * L"d")
    scatter!([sol.u[1]], [sol.u[2]], markercolor=:red, markersize=8, markerstrokewidth=0, label="maximum")
    plot!(legend_position=(0.09, 1.1), legend_column=2, topmargin=35Plots.px)
    return p
end

function __objective(alpha_val, d_val, sol::SOCPSolution; log=false)
    obj = @. bio_productivity(alpha_val', d_val)
    func = L"f_0^*(\alpha,d)"
    if log
        obj = Base.log.(obj)
        func = L"\ln " * func
    end

    p = contourf(alpha_val, d_val, obj, levels=25, color=:magma,
        linewidth=0.01, colorbar_title=func)
    plot!(alpha_val, d_washout.(alpha_val), color=:red, linewidth=3, label="washout")
    plot!(background_color_inside=:black)
    xlims!(0, 1)
    ylims!(0, 1.1 * d_val[end])
#    xlabel!("Optogenetic control " * L"\alpha")
#    ylabel!("Dilution rate " * L"d")
    scatter!([sol.u[1]], [sol.u[2]], markercolor=:red, markersize=8, markerstrokewidth=0, label="maximum")
#    plot!(legend_position=(0.09, 1.1), legend_column=2, topmargin=35Plots.px)
    return p
end

function double_plot(alpha_val, d_val, sol::SOCPSolution)
    p1 = __objective(alpha_val, d_val, sol, log=false)
    plot!(legend_position=(0.09, 1.1), legend_column=2, topmargin=35Plots.px)
    p2 = __objective(alpha_val, d_val, sol, log=true)
    plot!(legend=false)
    xlabel!("Optogenetic control " * L"\alpha")
    p = plot(p1, p2, layout=(2, 1), size=(500, 600))
    ylabel!("Dilution rate " * L"d")
end

function save_plot(sol::SOCPSolution, aa, dd; path::String="plots")
    if path[end] != '/'
        path *= "/"
    end
    mkpath(path)
    savefig(double_plot(aa, dd, sol), path * "socp_objective.pdf")
end

save_plot(sol, aa, dd, path="plots/")