
# switch functions
switch_alpha(x, λ) = (β*λ[3] - λ[2]) * ϕ(x[1])*x[2]
switch_d(x, λ, λ0) = λ[1]*(s_in - x[1]) - λ[2]*x[2] - λ[3]*x[3] + (λ0 - λ[5])*x[5]

# switch functions first derivatives
function d_switch_alpha_singular(x, λ)
    lsge = λ[1] / γ - λ[2]
    lvcq = λ[3] * x[5] - λ[4]
    return ( β * lvcq * d_ρ(x[3]) - lsge * ϕ(x[1]) ) * ϕ(x[1]) * x[2]
end

function d_switch_d(x, λ, λ0, α)
    lsge = λ[1] / γ - λ[2]
    lvcq = λ[3] * x[5] - λ[4]
    blve = λ[3] * β - λ[2]
    d_ϕ_tilde_s_e = (d2_ϕ(x[1]) * (s_in - x[1]) - d_ϕ(x[1])) * x[2]
    zd = lsge * d_ϕ_tilde_s_e - lvcq * d_ρ(x[3]) * x[3]
    zd += λ0 * μ(x[4]) * x[5] - blve * d_ϕ_tilde_s_e * α
    return zd
end

# α switch second derovative and α singular arc
function αs_coefs(x, λ)
    s, e, v, q, c, _ = x
    λs, λe, λv, λq, λc, _ = λ

    lsge = λs / γ - λe
    lvcq = λv * c - λq
    ϕ_tilde_s = d_ϕ(s) * (s_in - s)


    h0 = β * lvcq * ((d_ρ(v) * c + μ(q)) * d_ρ(v) - d2_ρ(v) * ρ(v) * c) - 
        β * (λq * q - λc * c) * d_μ(q) * d_ρ(v) + lsge * ϕ(s)^2
    ha = (β^2) * lvcq * d2_ρ(v) * ϕ(s) * e
    hd = -β * lvcq * d2_ρ(v) * v - lsge * (ϕ(s) + ϕ_tilde_s)
    return h0, ha, hd
end

function d2_switch_alpha_singular(x, λ, u)
    h0, ha, hd = αs_coefs(x, λ)
    return (h0 + ha*u[1] + hd*u[2]) * ϕ(x[1]) * x[2]
end

function α_singular(x, λ, d)
    h0, ha, hd = αs_coefs(x, λ)
    return -(h0 + hd*d) / ha
end

# d switch second derivative and d singular arc
function ds_coefs(x, λ, λ0)
    s, e, v, q, c, _ = x
    λs, λe, λv, λq, λc, _ = λ

    lsge = λs / γ - λe
    lvcq = λv * c - λq
    blve = λv * β - λe

    ϕ_tilde_s = d_ϕ(s) * (s_in - s)
    d_ϕ_tilde_s = d2_ϕ(s) * (s_in - s) - d_ϕ(s)
    ρ_tilde_v = d2_ρ(v) * v + d_ρ(v)
    ϕ_lie_s = d_ϕ(s) * ϕ_tilde_s - ϕ(s) * d_ϕ_tilde_s
    h_se = ϕ_lie_s * e^2 / γ

    g1 = lsge * h_se + lvcq * (ρ_tilde_v * ρ(v) * c - (d_ρ(v) * c + μ(q)) * d_ρ(v) * v) + 
        λ0 * (d_μ(q) * ρ(v) - d_μ(q) * μ(q) * q + μ(q)^2) * c + 
        (λq * q - λc * c) * d_μ(q) * d_ρ(v) * v
    g2 = -blve * h_se - β * lvcq * (ρ_tilde_v * ϕ(s) + d_ρ(v) * ϕ_tilde_s) * e
    g3 = lsge * d_ϕ_tilde_s * (s_in - s) * e + lvcq * ρ_tilde_v * v - λ0 * μ(q) * c
    g4 = -blve * d_ϕ_tilde_s * (s_in - s) * e

    return g1, g2, g3, g4
end

function d2_switch_d(x, λ, λ0, u)
    g1, g2, g3, g4 = ds_coefs(x, λ, λ0)
    return g1 + g2*u[1] + g3*u[2] + g4*u[1]*u[2]
end

function d_singular(x, λ, λ0, α)
    g1, g2, g3, g4 = ds_coefs(x, λ, λ0)
    return -(g1 + g2 * α) / (g3 + g4 * α)
end