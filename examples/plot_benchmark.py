import numpy as np
import csv

from examples.plot_utils import *


def load_array(path, silent=True):
    try:
        return np.loadtxt(path).T
    except FileNotFoundError:
        if not silent:
            print(f"File '{path}' not found, skipping.")
        return None
    return None

def plot_entry(id, base_path="results/benchmark"):
    entry = get_row_by_id(f"{base_path}/data.csv", id)
    if entry is None:
        raise ValueError(f"Entry #{id} not found.")
    
    plot_ocp_result(f"{base_path}/{id}", entry)
    return entry


def get_row_by_id(csv_file, id):
    with open(csv_file, mode="r", encoding="utf-8") as file:
        reader = csv.DictReader(file)
        for row in reader:
            if row["id"] == str(id):
                return dict(row)
    return None

def plot_ocp_result(path, entry):
    t = load_array(f"{path}/time.array")
    docp_x = load_array(f"{path}/docp_state.array")
    socp_x = load_array(f"{path}/socp_state.array")
    docp_u = load_array(f"{path}/docp_control.array")
    socp_u = load_array(f"{path}/socp_control.array")
    #x_eq = load_array(f"{path}/equilibrium.array")
    dmax = 1.5
    dswitch = entry["dmax_index"]
    dswitch = int(float(dswitch)) if len(dswitch) else 0

    if docp_x is not None and socp_x is not None:
        plot_state(t, docp_x, x_dotted=socp_x, path=f"{path}/state_trajectory.pdf")
    
    if entry['control'] != 'both':
        plot_one_control(t, docp_u, socp_u, entry['control'], dswitch, dmax,
                         path=f"{path}/control_trajectory.pdf")
    elif docp_u is not None and socp_u is not None:
        plot_control2(t, docp_u, socp_u, dswitch, dmax, path=f"{path}/control_trajectory.pdf")


if __name__ == '__main__':
    from sys import argv
    base_path = "results/benchmark"

    if len(argv) == 1:
        with open(f"{base_path}/data.csv", mode="r", encoding="utf-8") as file:
            reader = csv.DictReader(file)
            for row in reader:
                plot_entry(row["id"], base_path=base_path)
                plt.close('all')    # saves memory
    elif len(argv) == 2:
        id = argv[1]
        plot_entry(id, base_path=base_path)
    else:
        raise RuntimeError("Wrong number of arguments.")

