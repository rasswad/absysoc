# ABsysOC

Optimal control of an Algal-Bacterial (AB) consortium using
[JuMP.jl](https://jump.dev/) interface for the
[Ipopt](https://coin-or.github.io/Ipopt/) solver wrapped in the
[Ipopt.jl](https://github.com/jump-dev/Ipopt.jl) Julia package.

We provide the module and scripts that generate the plots in the article.

## Usage

To reproduce the plots of the article, make sure
[Julia](https://julialang.org/) is installed then

1. Clone this repository
2. Change into the cloned directory
3. Initialize `ABcontrol.jl` environment
4. Run [`examples/docp.jl`](examples/docp.jl) or
   [`examples/socp.jl`](examples/socp.jl)

From the shell terminal, run the following command

```sh
git clone https://gitlab.inria.fr/rasswad/absysoc.git ABsysOC
cd ABsysOC
```

Then launch the Julia console and run

```julia
# run SOCP script
include("examples/socp.jl");

# run DOCP script
include("examples/docp.jl")
```

It is possible to launch these scripts directly from the shell.
The dependencies need to be installed the first time from the Julia console
```julia
import Pkg
Pkg.activate(".")
```
then simply run the script from the shell
```sh
julia examples/socp.jl
```

## Package modules

Modules `rk.jl` and `docp.jl` provide *relatively* general functions for
solving dynamical optimal control problems (DOCP) that are not model-specific.

- [`docp.jl`](src/docp.jl) provides functions to construct and solve a DOCP
  through the direct method, based on the algorithmic guide by
  [Caillau et al. (2022)](https://doi.org/10.1016/bs.hna.2022.11.006).
- [`rk.jl`](src/rk.jl) provides functions to define a Runge-Kutta method
  for discretizing an ODE. Supports both time-invariant (autonomous)
  and time-varying systems.

Module `socp.jl` is a simple wrapper to define and solve a static optimal
control problem (SOCP), *i.e.* constant control optimization at steady state.

Modules `model.jl` and `plot.jl` are specific to the model we present
in the article.